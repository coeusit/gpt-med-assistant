class CreateMessages < ActiveRecord::Migration[7.1]
  def change
    create_table :messages do |t|
      t.string :uuid
      t.references :user, null: false, foreign_key: true
      t.text :content
      t.boolean :inbound

      t.timestamps
    end
  end
end
