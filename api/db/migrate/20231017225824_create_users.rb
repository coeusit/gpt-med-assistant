class CreateUsers < ActiveRecord::Migration[7.1]
  def change
    create_table :users do |t|
      t.string :uuid
      t.string :email
      t.string :password_digest
      t.text :history

      t.timestamps
    end
  end
end
