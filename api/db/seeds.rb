if User.count.zero?
    user = User.create(
        email: 'demo@example.com',
        password: 'password'
    )
    user.messages.create(
        content: "Welcome to the GPT medical assistant! To begin, do you have any medical history our GP office should be aware of?",
        inbound: false
    )
end