class Message < ApplicationRecord
  belongs_to :user
  after_save :push_to_user
  def push_to_user
    ApiSchema.subscriptions.trigger(
      :messages,
      {
      },
      {
        messages: [self]
      },
      scope: self.user.id
    )
  end
end
