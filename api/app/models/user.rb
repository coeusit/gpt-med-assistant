class User < ApplicationRecord
    has_secure_password
    has_many :messages

    def parse_history
        history_data = JSON.parse(self.history) rescue {}
        
        intake_form = Settings.intake_form.to_h
        
        parsed_history = intake_form.transform_values { "" }
        
        parsed_history.merge!(history_data)
    
        parsed_history
    end
end
