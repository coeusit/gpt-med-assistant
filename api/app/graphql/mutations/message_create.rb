# frozen_string_literal: true

module Mutations
  class MessageCreate < BaseMutation
    description "Creates a new message"

    field :message, Types::MessageType, null: false

    argument :message_input, Types::MessageInputType, required: true

    def resolve(message_input:)
      message_input_hash = message_input.to_h.merge(inbound: true)
      message = context[:current_user].messages.create(message_input_hash)
      raise GraphQL::ExecutionError.new "Error creating message", extensions: message.errors.to_hash unless message.save

      intake_form = Settings.intake_form.to_h
      parsed_history = context[:current_user].parse_history

      # Perform the analytical call
      prompt = "You are scanning user messages for medical history, for a medical intake form. You need to return the JSON object representing the user's data, amended with answers where applicable, or left empty otherwise.\n\nThe intake form:\n#{intake_form.to_json}\n\nCurrent user data:\n#{parsed_history.to_json}\n\nUser message:\n#{message.content}\n\nOutput:\n"

      client = OpenAI::Client.new

      response = client.completions(
        parameters: {
            model: "gpt-3.5-turbo-instruct",
            prompt: prompt,
            max_tokens: 512
        }
      )
      result = (response["choices"].map { |c| c["text"] }).first

      result_parsed = JSON.parse(result) rescue parsed_history

      context[:current_user].update!(history: result_parsed.to_json)

      # Analysis reconciled, now we produce a verbal response and continue our exchange with the patient.
      sys_msg = "You're a medical intake assistant for a GP office. You need to ask the patient about their medical history, which will be reconciled with their known data.\n\nIntake questions:\n#{intake_form.to_json}\n\nCurrent user data:\n#{result_parsed.to_json}"
      user_messages = context[:current_user].messages.order(created_at: :desc).limit(5).reverse
      messages_for_openai = user_messages[1..-1]
      user_messages_for_openai = messages_for_openai.map do |msg|
        { role: msg.inbound ? "user" : "assistant", content: msg.content }
      end

      response_message = context[:current_user].messages.create(
        inbound: false,
        content: ""
      )
      client.chat(
        parameters: {
            model: "gpt-3.5-turbo",
            messages: [
              { role: "system", content: sys_msg},
              *user_messages_for_openai,
              { role: "user", content: message.content }
            ],
            temperature: 0.7,
            stream: proc do |chunk, _bytesize|
              if !chunk.dig("choices", 0, "delta", "content").nil?
                response_message.update!(content: response_message.content + chunk.dig("choices", 0, "delta", "content"))
              end
            end
        }
      )

      { message: message }
    end
  end
end
