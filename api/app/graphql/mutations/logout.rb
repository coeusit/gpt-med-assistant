module Mutations
  class Logout < BaseMutation
    field :success, Boolean, null: false

    def resolve
      context[:session].deauth
      {
        success: true
      }
    end
  end
end
