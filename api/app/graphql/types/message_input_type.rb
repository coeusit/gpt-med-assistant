# frozen_string_literal: true

module Types
  class MessageInputType < Types::BaseInputObject
    argument :content, String, required: false
  end
end
