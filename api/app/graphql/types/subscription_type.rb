module Types
  class SubscriptionType < Types::BaseObject
    field :messages, subscription: Subscriptions::Messages
  end
end