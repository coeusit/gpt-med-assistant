# frozen_string_literal: true

module Types
  class UserType < Types::BaseObject
    field :uuid, String
    field :email, String
    field :history, String
  end
end
