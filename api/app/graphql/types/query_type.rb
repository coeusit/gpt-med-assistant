module Types
  class QueryType < Types::BaseObject
    include GraphQL::Types::Relay::HasNodeField
    include GraphQL::Types::Relay::HasNodesField

    field :current_session, resolver: Queries::CurrentSession
    field :my_data, resolver: Queries::MyData

    Dir[Rails.root.join('app/graphql/queries/**/*.rb')].sort.each do |file|
      require file
    end
  end
end