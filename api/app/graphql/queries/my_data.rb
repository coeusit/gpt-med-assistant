module Queries
    class MyData < GraphQL::Schema::Resolver
        description "User data"

        type Types::UserType, null: false

        def resolve
            context[:current_user]
        end
    end
end