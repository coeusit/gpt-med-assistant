module Subscriptions
    class Messages < BaseSubscription
        field :messages, [Types::MessageType], null: true
        subscription_scope :current_user_id

        def subscribe
            {
                messages: context[:current_user].messages.all
            }
        end
    end
end