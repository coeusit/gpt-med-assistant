Sample GPT medical intake assistant with streaming chat and persistent memory.

To install the Docker-compose stack, execute this from the project root:
```
docker-compose -f docker-compose.development.yml up -d
```

To prepare the database, run this from the shell of the **api** container
```
rake db:migrate
rake db:seed
```

To run the client application in Quasar's development mode, execute this from the client directory:
```
GRAPHQL_URI=http://localhost:3020/graphql GRAPHQL_URI_WS=ws://localhost:3020/cable quasar dev
```
**or** run the following command in the client directory:
```
sh dev.sh
```

Ensure your OpenAI API key is set before you set up the Docker stack. To do so, open /docker-conf/.env.development, and set "OPENAI_KEY" to your API key.