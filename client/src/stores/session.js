import { defineStore } from 'pinia'

export const useSessionStore = defineStore('session', {
  state: () => ({
    initialized: false,
    authenticated: false,
    email: '',
    uuid: null
  }),

  getters: {
  },

  actions: {
    setUser(user) {
      this.authenticated = user !== null
      this.email = user?.email || null
      this.uuid = user?.uuid || null
    },
    setInitialized() {
      this.initialized = true
    }
  }
})
