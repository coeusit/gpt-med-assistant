import gql from 'graphql-tag'

const MessageCreateMutation = gql`
  mutation ($messageInput: MessageInput!) {
    messageCreate(input: { messageInput: $messageInput }) {
      message {
        uuid
      }
    }
  }
`
export default MessageCreateMutation
