import { gql } from 'graphql-tag';

const MessagesSubscription = gql`
  subscription {
    messages {
      messages {
        uuid
        content
        inbound
        updatedAt
      }
    }
  }
`;

export default MessagesSubscription;
