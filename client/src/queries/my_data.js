import { gql } from 'graphql-tag';

const MyDataQuery = gql`
  query {
    myData {
      uuid
      email
      history
    }
  }
`;

export default MyDataQuery;
